
#include "smash_tests.h"

void init_simple_command(char* lineSize, char* cmdString) {
    strcpy(cmdString, lineSize);
    cmdString[strlen(lineSize)-1]='\0';
}


void test_pwd_1() {

    char lineSize[MAX_LINE_SIZE] = "pwd\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);

    int res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==0);

}

void test_pwd_2() {

    char lineSize[MAX_LINE_SIZE] = "pwd 1\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);

    int res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==1);

}

void test_cd() {

    char lineSize[MAX_LINE_SIZE] = "cd \n";// suppose to fail to few arguments
    char cmdString[MAX_LINE_SIZE];
    char pwd[MAX_LINE_SIZE];
    char Lpwd[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);

    int res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,"cd /home 12");// suppose to fail too many arguments
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,"cd /home12");// suppose to fail path not exist
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==1);

    getcwd(Lpwd, MAX_LINE_SIZE);
    strcpy(lineSize,"cd /home/os");// suppose to succeed
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,"/home/os"));

    strcpy(lineSize,"cd -");// suppose to succeed and return us to the last location
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,Lpwd));

    strcpy(lineSize,"cd -");// suppose to succeed and return us to the last location
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,"/home/os"));

    strcpy(lineSize,"cd -");// suppose to succeed and return us to the last location
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,Lpwd));

}

void test_showpid(){
    char lineSize[MAX_LINE_SIZE] = "showpid 1\n"; //too many arguments
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);

    int res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize, "showpid");
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(NULL, lineSize, cmdString);
    assert(res==0);

}

void run_smash_tests() {
    test_pwd_1();
    test_pwd_2();
    test_cd();
    test_showpid();
    printf("all tests passed\n");

}